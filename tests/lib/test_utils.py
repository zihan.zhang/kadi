# Copyright 2020 Karlsruhe Institute of Technology
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
from kadi.lib.utils import find_dict_in_list
from kadi.lib.utils import get_class_by_name
from kadi.lib.utils import is_iterable
from kadi.lib.utils import named_tuple
from kadi.lib.utils import parse_datetime_string
from kadi.lib.utils import rgetattr
from kadi.lib.utils import SimpleReprMixin
from kadi.modules.records.models import Record


def test_simple_repr_mixin():
    """Test if the "SimpleReprMixin" works correctly."""

    class _Test(SimpleReprMixin):
        # pylint: disable=missing-class-docstring
        class Meta:
            representation = ["a", "b"]

        a = 1
        b = 2

    assert repr(_Test()) == "_Test(a=1, b=2)"


def test_named_tuple():
    """Test if the "named_tuple" shortcut works correctly."""
    test_tuple = named_tuple("Test", a=1, b=2)

    assert test_tuple.a == 1
    assert test_tuple.b == 2


def test_find_dict_in_list():
    """Test if dictionaries in lists are found correctly."""
    dict_list = [{"a": 1}, {"b": None}]
    assert find_dict_in_list(dict_list, "a", 1)
    assert find_dict_in_list(dict_list, "b", None)
    assert not find_dict_in_list(dict_list, "c", None)


def test_rgetattr():
    """Test if recursively getting an attribute works correctly."""
    test_obj = named_tuple("Test", a=named_tuple("Test2", b="test"))

    assert rgetattr(test_obj, "a.b") == "test"
    assert rgetattr(test_obj, "a.b.c", "default") == "default"


def test_get_class_by_name():
    """Test if getting a class by its name works correctly."""
    assert get_class_by_name("kadi.modules.records.models.Record") == Record
    assert get_class_by_name("kadi.invalid.module") is None


def test_is_iterable():
    """Test if iterables are detected correctly."""
    assert not is_iterable(1)
    assert not is_iterable("test")
    assert is_iterable("test", include_string=True)
    assert is_iterable([])
    assert is_iterable({})


def test_parse_datetime_string():
    """Test if datetime strings are parsed correctly."""
    date_time = parse_datetime_string("2020-01-01T12:34:56.000Z")

    assert parse_datetime_string("test") is None
    assert parse_datetime_string("2020-01-01T12:34:56Z") == date_time
    assert parse_datetime_string("2020-01-01T12:34:56+00:00") == date_time
    assert parse_datetime_string("2020-01-01T12:34:56.00000Z") == date_time
    assert parse_datetime_string("2020-01-01T12:34:56.00000+00:00") == date_time
