# Copyright 2020 Karlsruhe Institute of Technology
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
from kadi.lib.api.core import create_access_token
from kadi.lib.api.core import json_error_response
from kadi.lib.api.core import json_response
from kadi.lib.api.models import AccessToken
from kadi.lib.utils import utcnow
from kadi.lib.web import url_for
from tests.utils import check_api_response


def test_create_access_token(dummy_user):
    """Test if access tokens are created correctly."""
    token = AccessToken.new_token()
    access_token = create_access_token(
        name="test",
        user=dummy_user,
        expires_at=utcnow(),
        token=token,
        scopes=["user.read", "record.read"],
    )

    assert AccessToken.query.filter_by(name="test").first().id == access_token.id
    assert AccessToken.filter_by_token(token).id == access_token.id
    assert access_token.is_expired
    assert access_token.scopes.count() == 2
    assert access_token.scopes.filter_by(object="user").first().action == "read"


def test_json_response(api_client, dummy_access_token):
    """Test if JSON responses work correctly."""
    response = json_response(200, {"test": "test"}, headers={"test": "test"})

    check_api_response(response)
    assert "test" in response.get_json()
    assert "test" in response.headers


def test_json_error_response(api_client, dummy_access_token):
    """Test if JSON error responses work correctly."""
    response = json_error_response(
        404, message="test", description="test", test="test", headers={"test": "test"}
    )
    data = response.get_json()

    check_api_response(response, status_code=404)
    assert "test" in response.headers
    assert "code" in data
    assert "test" in data["message"]
    assert "test" in data["description"]
    assert "test" in data["test"]

    # Check default information.
    response = json_error_response(404)
    data = response.get_json()

    check_api_response(response, status_code=404)
    assert "code" in data
    assert "message" in data
    assert "description" in data


def test_internal_endpoint(api_client, client, dummy_access_token, user_session):
    """Test if internal endpoints work correctly."""
    endpoint = url_for("api.search_resources")

    with user_session():
        response = client.get(endpoint)
        check_api_response(response)

    response = api_client(dummy_access_token).get(endpoint)
    check_api_response(response, status_code=404)


def test_scopes_required(api_client, client, new_access_token, user_session):
    """Test if scopes of access tokens work correctly."""
    endpoint = url_for("api.get_users")

    # Check if a token with a wrong scope does not work. Has to be done manually, as
    # this scope is normally not valid.
    token = new_access_token(scopes=["record.read"])
    response = api_client(token).get(endpoint)

    check_api_response(response, status_code=401)
    assert "Access token has insufficient scope." in response.get_json()["description"]

    # Check if setting the scope explicitely works.
    token = new_access_token(scopes=["user.read"])

    response = api_client(token).get(endpoint)
    check_api_response(response)

    # Check if a token with full access (i.e. without scopes) works.
    token = new_access_token()

    response = api_client(token).get(endpoint)
    check_api_response(response)

    # Check if using the session also works.
    with user_session():
        response = client.get(endpoint)
        check_api_response(response)
