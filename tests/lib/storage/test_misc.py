# Copyright 2020 Karlsruhe Institute of Technology
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
import os
from uuid import uuid4

from flask import current_app

from kadi.lib.storage.misc import create_misc_uploads_path
from kadi.lib.storage.misc import delete_thumbnail
from kadi.lib.storage.misc import save_as_thumbnail


def test_save_as_thumbnail(monkeypatch, tmp_path, dummy_image):
    """Test if saving an image as thumbnail works correctly."""
    monkeypatch.setitem(current_app.config, "MISC_UPLOADS_PATH", tmp_path)

    identifier = str(uuid4())
    save_as_thumbnail(identifier, dummy_image)

    assert os.listdir(tmp_path)[0] == identifier[:2]
    assert os.path.isfile(create_misc_uploads_path(identifier))


def test_delete_thumbnail(monkeypatch, tmp_path, dummy_image):
    """Test if deleting a thumbnail works correctly."""
    monkeypatch.setitem(current_app.config, "MISC_UPLOADS_PATH", tmp_path)

    identifier = str(uuid4())
    save_as_thumbnail(identifier, dummy_image)
    delete_thumbnail(identifier)

    assert not os.listdir(tmp_path)
