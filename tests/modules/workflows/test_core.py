# Copyright 2021 Karlsruhe Institute of Technology
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
from flask import current_app

from kadi.modules.workflows.core import parse_tool_file


def test_parse_tool(monkeypatch, tmp_path, new_file):
    """Test if tools are parsed correctly."""
    monkeypatch.setitem(current_app.config, "STORAGE_PATH", tmp_path)

    file = new_file(
        magic_mimetype="application/x-tool+xml",
        file_data=b"""
        <program name="test" version="1.0.0">
            <param name="test" type="test" char="t" required="true"></param>
        </program>
        """,
    )
    assert parse_tool_file(file) == {
        "name": "test",
        "version": "1.0.0",
        "param": [{"name": "test", "type": "test", "char": "t", "required": True}],
    }
