# Copyright 2020 Karlsruhe Institute of Technology
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
import pytest
from flask import current_app

from kadi.lib.web import url_for
from tests.modules.records.utils import initiate_upload
from tests.modules.records.utils import upload_chunk
from tests.utils import check_api_response


def test_edit_file_data_success(
    monkeypatch, api_client, dummy_access_token, dummy_file, dummy_record
):
    """Test the success behaviour of the "api.edit_file_data" endpoint."""
    monkeypatch.setitem(current_app.config, "MAX_UPLOAD_USER_QUOTA", 10)

    response = initiate_upload(
        api_client(dummy_access_token),
        url_for("api.edit_file_data", record_id=dummy_record.id, file_id=dummy_file.id),
        file_data=10 * b"x",
        replaces_file=True,
    )
    check_api_response(response, status_code=201)


@pytest.mark.parametrize(
    "conf_key,error_status", [("MAX_UPLOAD_SIZE", 413), ("MAX_UPLOAD_USER_QUOTA", 413)]
)
def test_edit_file_data_error(
    conf_key,
    error_status,
    monkeypatch,
    tmp_path,
    api_client,
    db,
    dummy_access_token,
    dummy_record,
    new_file,
):
    """Test the error behaviour of the "api.edit_file_data" endpoint."""
    monkeypatch.setitem(current_app.config, "STORAGE_PATH", tmp_path)
    monkeypatch.setitem(current_app.config, conf_key, 10)

    file = new_file(file_data=10 * b"")

    response = initiate_upload(
        api_client(dummy_access_token),
        url_for("api.edit_file_data", record_id=dummy_record.id, file_id=file.id),
        replaces_file=True,
        file_data=11 * b"x",
    )
    check_api_response(response, status_code=error_status)


def test_upload_chunk(
    monkeypatch, tmp_path, api_client, dummy_access_token, dummy_record
):
    """Test the "api.upload_chunk" endpoint."""
    monkeypatch.setitem(current_app.config, "STORAGE_PATH", tmp_path)
    monkeypatch.setitem(current_app.config, "UPLOAD_CHUNK_SIZE", 10)

    client = api_client(dummy_access_token)
    file_data = 15 * b"x"

    data = initiate_upload(
        client, url_for("api.new_upload", id=dummy_record.id), file_data=file_data
    ).get_json()

    for index in range(data["chunk_count"]):
        chunk_size = current_app.config["UPLOAD_CHUNK_SIZE"]
        chunk_data = file_data[index * chunk_size : (index + 1) * chunk_size]

        response = upload_chunk(
            client, data["_actions"]["upload_chunk"], chunk_data=chunk_data, index=index
        )
        check_api_response(response)
