# Copyright 2020 Karlsruhe Institute of Technology
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
import zipfile
from io import BytesIO

from kadi.lib.web import url_for
from kadi.modules.records.previews import get_preview_data


def test_get_preview_data(dummy_file, new_file):
    """Test if determining the preview data of files works correctly.

    Based on the builtin preview data as well as the textual fallback.
    """
    text_data = 10 * b"x"
    zip_buffer = BytesIO()
    with zipfile.ZipFile(zip_buffer, mode="w", compression=zipfile.ZIP_DEFLATED):
        pass

    zip_file = new_file(file_data=zip_buffer.getvalue())
    text_file = new_file(file_data=text_data)
    empty_file = new_file(file_data=b"")

    assert get_preview_data(dummy_file) == (
        "image",
        url_for(
            "api.preview_file", record_id=dummy_file.record.id, file_id=dummy_file.id
        ),
    )
    assert get_preview_data(zip_file) == ("archive", [])
    assert get_preview_data(text_file) == (
        "text;ascii",
        text_data.decode(encoding="ascii"),
    )
    assert get_preview_data(empty_file) is None
