# Copyright 2020 Karlsruhe Institute of Technology
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
from kadi.lib.web import url_for
from tests.utils import check_view_response


def test_index(client, user_session):
    """Test the "main.index" endpoint."""
    endpoint = url_for("main.index")

    response = client.get(endpoint)
    check_view_response(response)

    with user_session():
        response = client.get(endpoint)
        check_view_response(response)


def test_about(client):
    """Test the "main.about" endpoint."""
    response = client.get(url_for("main.about"))
    check_view_response(response)


def test_help(client):
    """Test the "main.help" endpoint."""
    response = client.get(url_for("main.help"))
    check_view_response(response)
