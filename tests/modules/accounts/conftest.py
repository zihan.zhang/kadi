# Copyright 2020 Karlsruhe Institute of Technology
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
import pytest

from kadi.lib.utils import named_tuple
from kadi.modules.accounts.providers.ldap import LDAPProvider
from kadi.modules.accounts.providers.shib import ShibProvider


@pytest.fixture(autouse=True)
def _auth_providers(monkeypatch):
    """Fixture to patch the authentication providers relying on external systems.

    Will patch the providers in all relevant modules where they are used.

    Executed automatically for each test.
    """

    class _LDAPProvider(LDAPProvider):
        @classmethod
        def authenticate(cls, *, username, password, **kwargs):
            ldap_info = named_tuple("LDAPInfo", username="", email="", displayname="")
            return cls.UserInfo(True, ldap_info)

    class _ShibProvider(ShibProvider):
        @classmethod
        def contains_valid_idp(cls):
            return True

        @classmethod
        def change_password(cls, username, old_password, new_password):
            raise NotImplementedError

        @classmethod
        def authenticate(cls, **kwargs):
            shib_info = named_tuple("ShibInfo", username="", email="", displayname="")
            return cls.UserInfo(True, shib_info)

    monkeypatch.setattr("kadi.modules.accounts.views.LDAPProvider", _LDAPProvider)
    monkeypatch.setattr("kadi.modules.accounts.views.ShibProvider", _ShibProvider)
