/* Copyright 2021 Karlsruhe Institute of Technology
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. */

import * as d3 from 'd3';

const url = d3.select('#target-svg').attr('data');

d3.json(url).then((data) => {
  const svg = d3.select('#target-svg');
  const width = Number(svg.attr('width'));
  const height = Number(svg.attr('height'));
  svg.attr('preserveAspectRatio', 'xMidYMid meet')
    .attr('viewBox', [0, 0, width * 2, height * 2]);

  const graphContainer = svg.append('g').attr('class', 'graphContainer');

  graphContainer.append('defs').append('marker')
    .attr('id', 'arrowhead')
    .attr('viewBox', '-0 -5 10 10')
    // x coordinate for the reference point of the marker.
    // If circle is bigger, this need to be bigger.
    .attr('refX', 36)
    .attr('refY', 0)
    .attr('orient', 'auto')
    .attr('markerWidth', 6)
    .attr('markerHeight', 6)
    .attr('xoverflow', 'visible')
    .append('svg:path')
    .attr('d', 'M 0,-5 L 10 ,0 L 0,5')
    .attr('fill', '#999')
    .style('stroke', 'none');

  const zoomF = d3.zoom()
    .scaleExtent([1 / 4, 8])
    .on('zoom', (event) => {
      graphContainer.attr('transform', event.transform);
    });

  svg.call(zoomF);

  const links = data.links.map((d) => Object.create(d));// make links an empty array with the some form as links
  links.forEach((d, i) => {
    d.srcType = data.links[i].srcType;
    d.tgtType = data.links[i].tgtType;
    d.relationship = data.links[i].relationship;
  });

  const nodes = data.nodes.map((d) => Object.create(d));
  nodes.forEach((d, i) => {
    d.type = data.nodes[i].type || 'None';// when the type of record is null, then set it to "none"
    d.url = data.nodes[i].url;
    d.id = data.nodes[i].id;
    d.identifier = data.nodes[i].identifier;
  });

  const nodesCount = {};

  nodes.forEach((el) => {
    nodesCount[el.type] = (nodesCount[el.type] || 0) + 1;
  });

  const uniqueTypes = Object.keys(nodesCount);

  // set colors for different type of nodes
  // d3.schemePaired return an array of 12 categorical colors
  // more general solution when category of "type" is more than 12?
  const colorScale = d3.scaleOrdinal().domain(uniqueTypes).range(d3.schemePaired);

  function countSiblingLinks(source, target) {
    let count = 0;
    for (let i = 0; i < links.length; ++i) {
      if ((links[i].source.id === source.id && links[i].target.id === target.id)
        || (links[i].source.id === target.id && links[i].target.id === source.id)) {
        count++;
      }
    }
    return count;
  }

  function getSiblingLinks(source, target) {
    const siblings = [];
    for (let i = 0; i < links.length; ++i) {
      if ((links[i].source.id === source.id && links[i].target.id === target.id)
          || (links[i].source.id === target.id && links[i].target.id === source.id)) {
        siblings.push(links[i].relationship);
      }
    }
    return siblings;
  }

  function arcPath(leftHand, d) {
    /* eslint-disable sort-vars */
    const x1 = leftHand ? d.source.x : d.target.x,
      y1 = leftHand ? d.source.y : d.target.y,
      x2 = leftHand ? d.target.x : d.source.x,
      y2 = leftHand ? d.target.y : d.source.y,
      dx = x2 - x1,
      dy = y2 - y1,
      dr = Math.sqrt((dx * dx) + (dy * dy)),
      drx = dr,
      // eslint-disable-next-line no-unused-vars
      dry = dr,
      sweep = leftHand ? 0 : 1,
      siblingCount = countSiblingLinks(d.source, d.target),
      xRotation = 0,
      largeArc = 0;
    /* eslint-disable sort-vars */

    if (siblingCount > 1) {
      const siblings = getSiblingLinks(d.source, d.target);
      const arcScale = d3.scaleOrdinal()
        .domain(siblings)
        .range([1, siblingCount]);
      // eslint-disable-next-line no-const-assign
      drx /= (1 + ((1 / siblingCount) * (arcScale(d.relationship) - 1)));
      // eslint-disable-next-line no-const-assign
      dry /= (1 + ((1 / siblingCount) * (arcScale(d.relationship) - 1)));

      return `M${x1},${y1}A${drx}, ${dry} ${xRotation}, ${largeArc}, ${sweep} ${x2},${y2}`;
    }
    return `M ${d.source.x} ${d.source.y} L ${d.target.x} ${d.target.y}`;
  }

  function hiddenUnselectedNodes(selectedType) {
    d3.selectAll('.node_el').filter(function(d) {
      return d.type !== selectedType;
    })
      .style('visibility', 'hidden');
    d3.selectAll('.linkpath').style('visibility', 'hidden');
    d3.selectAll('.link').style('visibility', 'hidden');
    d3.selectAll('.linklabel').style('visibility', 'hidden');
    d3.selectAll('.legend_block').filter(function(d) {
      return d !== selectedType;
    })
      .attr('pointer-events', 'none');
    d3.selectAll('.legend_block').style(
      'opacity',
      function(e) {
        return (e === selectedType) ? 1.0 : 0.3;
      },
    );
    d3.selectAll('.legend_text').style(
      'opacity',
      function(e) {
        return (e === selectedType) ? 1.0 : 0.3;
      },
    );
  }

  function visibleUnselectedNodes(selectedType) {
    d3.selectAll('.node_el').filter(function(d) {
      return d.type !== selectedType;
    })
      .style('visibility', 'visible');
    d3.selectAll('.linkpath').style('visibility', 'visible');
    d3.selectAll('.link').style('visibility', 'visible');
    d3.selectAll('.linklabel').style('visibility', 'visible');
    d3.selectAll('.legend_block').filter(function(d) {
      return d !== selectedType;
    })
      .attr('pointer-events', 'auto');
    d3.selectAll('.legend_block').style(
      'opacity',
      function() {
        return 1;
      },
    );
    d3.selectAll('.legend_text').style(
      'opacity',
      function() {
        return 1;
      },
    );
  }

  function focusNode() {
    d3.select(this)
      .select('circle')
      .style('stroke', '#3f51b5')
      .style('stroke-width', 6);
  }

  function blurNode() {
    d3.select(this)
      .select('circle')
      .style('stroke', 'none')
      .style('stroke-width', 0);
  }

  // color legend
  const size = 40;
  const startPoint = 40;

  const ColorLegend = svg.append('g')
    .attr('class', 'ColorLegends')
    .selectAll('ColorLegend')
    .data(uniqueTypes)
    .enter();

  const colorBlock = ColorLegend
    .append('rect')
    .attr('class', 'legend_block')
    .attr('x', startPoint)
    .attr('y', function(d, i) {
      return startPoint + (i * (size + 5));
    })
    .attr('width', size)
    .attr('height', size)
    .attr('opacity', 1)
    .attr('type', (d) => d)
    .style('fill', function(d) {
      if (d !== 'None') {
        return colorScale(d);
      }
      return '#918e8a';// grey
    });

  // colorText
  ColorLegend
    .append('text')
    .style('font-size', '25px')
    .attr('class', 'legend_text')
    .style('text-anchor', 'start')
    .style('alignment-baseline', 'middle')
    .attr('x', startPoint + (size * 1.2))
    .attr('y', function(d, i) {
      return startPoint + (i * (size + 5)) + (0.5 * size);
    })
    .attr('opacity', 1)
    .attr('type', (d) => d)
    .style('fill', function(d) {
      if (d !== 'None') {
        return colorScale(d);
      }
      return '#918e8a';// grey
    })
    .text((d) => `${d} : ${nodesCount[d]}`);

  // filter the nodes according to their record type
  colorBlock.on('click', function(d) {
    const selectedType = d.target.__data__;
    const visibleFlag = (d3.selectAll('.node_el').filter(function(d) {
      return d.type === selectedType;
    })
      .style('visibility') === 'visible') && (d3.selectAll('.node_el').filter(function(d) {
      return d.type !== selectedType;
    })
      .style('visibility') === 'visible');
    if (visibleFlag) {
      return hiddenUnselectedNodes(selectedType);
    }

    return visibleUnselectedNodes(selectedType);
  });

  const simulation = d3.forceSimulation(nodes)
    .force('link', d3.forceLink(links)
      .id((d) => d.id)
      .distance(300))
    .force('charge', d3.forceManyBody()// charge force,
      .strength(-1500)
      .distanceMin(100)
      .distanceMax(350))
    .force('collision', d3.forceCollide()// collision force, prevent the overlapping
      .radius(70).iterations(2))
    .force('center', d3.forceCenter(width, height));

  // drag movement
  const drag = (simulation) => {
    function dragstarted(event) {
      if (!event.active) {
        simulation.alphaTarget(0.5).restart();
      }
      event.subject.fx = event.subject.x;
      event.subject.fy = event.subject.y;
    }
    function dragged(event) {
      event.subject.fx = event.x;
      event.subject.fy = event.y;
    }
    function dragended(event) {
      if (!event.active) {
        simulation.alphaTarget(0);
      }
      event.subject.fx = null;
      event.subject.fy = null;
    }
    return d3.drag()
      .on('start', dragstarted)
      .on('drag', dragged)
      .on('end', dragended);
  };

  const link = graphContainer.append('g')
    .attr('class', 'link')
    .selectAll('line')
    .data(links)
    .enter()
    .append('path')
    .attr('fill', 'none')
    .attr('stroke', '#999')
    .attr('stroke-opacity', 0.6)
    .attr('stroke-width', 3)
    .attr('id', (d, i) => `linkId_${i}`)
    // make path go along with the link provide position for link label
    .attr('marker-end', 'url(#arrowhead)');


  const linkPath = graphContainer.append('g')
    .attr('class', 'linkpath')
    .selectAll('linkpath')
    .data(links)
    .enter()
    .append('path')
    .attr('stroke', 'black')
    .attr('stroke-width', '2px')
    .attr('fill-opacity', 0)
    .attr('stroke-opacity', 0)
    .attr('id', (d, i) => `linkPathId_${i}`)
    .style('pointer-events', 'none');

  const linkLabel = graphContainer.append('g')
    .attr('class', 'linklabel')
    .selectAll('linklabel')
    .data(links)
    .enter()
    .append('text')
    .style('pointer-events', 'none')
    .attr('id', (d, i) => {
      return `linklabel_${i}`;
    })
    .attr('dx', 0)
    .attr('dy', '1.35em')
    .style('fill', 'black');

  // To render text along the shape of a <path>
  // enclose the text in a <textPath> element that has an href attribute with a reference to the <path> element.
  linkLabel
    .append('textPath')
    .attr('xlink:href', function(d, i) {
      return `#linkPathId_${i}`;
    })
    .style('text-anchor', 'middle')
    .style('pointer-events', 'none')
    .attr('startOffset', '50%')
    .text((d) => d.relationship);

  const node = graphContainer
    .append('g')
    .attr('class', 'node')
    .selectAll('g')
    .data(nodes)
    .enter()
    .append('g')
    .attr('class', 'node_el')
    .attr('type', (d) => d.type);

  // render node as circle
  node
    .append('circle')
    .attr('r', 50)
    .attr('fill', function(d) {
      if (d.type !== 'None') {
        return colorScale(d.type);
      }
      return '#918e8a';// grey
    });

  // label text on node
  node
    .append('text')
    .attr('dy', '0.3em')
    .style('fill', '#555')
    .text((d) => d.identifier);

  // handler on node
  node
    .on('mouseover', focusNode)
    .on('mouseout', blurNode)
    .on('dblclick', (d) => {
      // open a new tab which links to the record page in kadi
      window.open(d.srcElement.__data__.url, '_blank');
    })
    .call(drag(simulation));


  simulation.on('tick', () => {
    link
      .attr('d', (d) => arcPath(true, d));

    node
      .attr('transform', (d) => `translate(${d.x},${d.y})`);

    linkPath
      .attr('d', (d) => arcPath(true, d));

    linkLabel
      .attr('transform', function(d) {
        if (d.target.x < d.source.x) {
          const bbox = this.getBBox(),
            rx = bbox.x + (bbox.width / 2),
            ry = bbox.y + (bbox.height / 2);
          return `rotate(180,${rx}, ${ry})`;
        }
        return 'rotate(0)';
      });
  });
});

new Vue({
  el: '#vm',
  data: {
    requestInProgress: false,
  },
  methods: {
    downloadFiles() {
      this.requestInProgress = true;
      axios.post(kadi.js_resources.download_files_endpoint)
        .then(() => kadi.getNotifications())
        .catch((error) => {
          if (error.request.status === 429) {
            // Use the error message from the backend.
            kadi.alert(error.response.data.description, {type: 'info'});
          } else {
            kadi.alert(i18n.t('error.packageFiles'), {xhr: error.request});
          }
        })
        .finally(() => this.requestInProgress = false);
    },
    deleteFile(file) {
      if (!confirm(i18n.t('warning.deleteIdentifier', {identifier: file.name}))) {
        return;
      }

      this.$set(file, 'disabled', true);

      axios.delete(file._actions.delete)
        .then(() => {
          this.$refs.filesPagination.update();
          this.$refs.fileRevisionsPagination.update();
          kadi.alert(i18n.t('success.deleteFile'), {type: 'success', scrollTo: false});
        })
        .catch((error) => {
          kadi.alert(i18n.t('error.deleteFile'), {xhr: error.request});
          file.disabled = false;
        });
    },
  },
});
