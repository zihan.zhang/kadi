Hello {{ displayname }},

to reset your password please paste the following link into your browser's address bar:
{{ url_for("accounts.reset_password", token=token, _external=True) }}

If you did not request a password reset you can safely ignore this email.
