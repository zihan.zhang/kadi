#!/usr/bin/env bash

pip install -e .

kadi assets dev
kadi db init
kadi i18n compile

ES_HOST=$(kadi utils config | grep ELASTICSEARCH_HOSTS | sed -E "s/^ELASTICSEARCH_HOSTS: //")

while [[ -z "$(curl -sf ${ES_HOST}/_cluster/health)" ]]; do
  sleep 1
done

kadi search init

flask run --host=0.0.0.0
