.. development:

Development
===========

This chapter covers various information about developing or contributing to
Kadi4Mat. Before reading, make sure you have a working :ref:`development
environment <installation-development>`, preferably using the manual
installation.

.. toctree::
   :maxdepth: 2

   General <general>
   Style guide <style_guide>
   Testing <testing>
   Plugins <plugins>
   Translations <translations>
   Documentation <documentation>
   Todos <todos>
