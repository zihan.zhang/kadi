Production
==========

This section describes how to install and update Kadi4Mat in a production
environment. There are currently two ways to do this: installation via setup
script and manual installation. When using the former, it is still recommended
to check out the manual installation instructions, as all the necessary steps
are described in much more detail.

.. note::

  The described steps currently only cover installing and setting up the
  application. Keep in mind that, once deployed, there are other aspects to
  consider as well, e.g. securing the server and services by keeping packages
  up to date, using a firewall, etc.

  It is also important to regularly create backups of at least the database and
  locally stored files. The former can be done using ``pg_dump`` (as PostgreSQL
  is used as an RDBMS), while the latter depends on which file paths have been
  set in the Kadi4Mat configuration value to store local files
  (``STORAGE_PATH`` and ``MISC_UPLOADS_PATH``), as explained in
  :ref:`Configuration <installation-configuration>`.

.. toctree::
   :maxdepth: 2

   Installation via setup script <script>
   Manual installation <manual>
   Updating the application <updating>
