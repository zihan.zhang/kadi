Manual installation
===================

The described installation steps are supposed to be run on a freshly installed
server or virtual machine running a current version of Debian or Ubuntu.

.. include:: ../_dependencies.rst

uWSGI
~~~~~

`uWSGI <https://uwsgi-docs.readthedocs.io/en/latest/>`__ is an application
server implementing the WSGI interface, used to serve the actual Python
application. It can be installed like this:

.. code-block:: bash

  sudo apt install uwsgi uwsgi-plugin-python3

The python3 plugin is needed as well, since the distro-supplied uWSGI package
is built in a modular way.

Apache
~~~~~~

The `Apache HTTP Server <https://httpd.apache.org>`__ is used as a reverse
proxy server in front of uWSGI, handling the actual HTTP requests. The server
as well as all additionally required modules can be installed like this:

.. code-block:: bash

  sudo apt install apache2 libapache2-mod-proxy-uwsgi libapache2-mod-xsendfile

Installing Kadi4Mat
-------------------

Before installing the application, it is best to create a dedicated user that
the application and all required services will run under:

.. code-block:: bash

  sudo adduser kadi --system --home /opt/kadi --ingroup www-data --shell /bin/bash

As some later steps require root privileges again, all commands that require
the newly created user are prefixed with ``kadi $``. Therefore it is best to
switch to the new user using a separate terminal window:

.. code-block:: bash

  sudo su - kadi

To create and activate a new virtual environment for the application, the
following commands can be used:

.. code-block:: bash

  kadi $ virtualenv -p python3 ${HOME}/venv
  kadi $ source ${HOME}/venv/bin/activate

This will create and activate a new virtual environment named *venv* using
Python 3 as interpreter. For all following steps requiring the newly created
user, the virtual environment is assumed to be active. Afterwards, the
application can be installed like this:

.. code-block:: bash

  kadi $ pip install kadi

Configuration
-------------

Postgres
~~~~~~~~

To set up Postgres, a user and a database belonging to that user have to be
created:

.. code-block:: bash

  sudo -u postgres createuser -P kadi
  sudo -u postgres createdb -O kadi kadi -E utf-8

.. _installation-production-manual-configuration-kadi4mat:

Kadi4Mat
~~~~~~~~

While most of the application configuration values have usable defaults set,
some values need to be set explicitly when using a production environment. For
this, a separate configuration file has to be created, for example:

.. code-block:: bash

  kadi $ mkdir ${HOME}/config
  kadi $ touch ${HOME}/config/kadi.py
  kadi $ chmod 640 ${HOME}/config/kadi.py

This file can be treated like a normal Python file, i.e. calculating values or
importing other modules will work, however, the syntactical rules and
formatting of Python also apply. It is important for all services that need
access to the application configuration, but also for using the Kadi command
line interface (CLI). The Kadi CLI offers some useful tools and utility
functions running in the context of the application (see also :ref:`Command
line interfaces <development-general-cli>`). As such, it also needs access to
the configuration file, which can be done setting the appropriate environment
variable:

.. code-block:: bash

  kadi $ export KADI_CONFIG_FILE=${HOME}/config/kadi.py

The above line could also be added to :file:`.profile` or a similar
configuration file for convenience, so it is always executed when switching to
the new user, together with activating the virtual environment:

.. code-block:: bash

  kadi $ echo 'export KADI_CONFIG_FILE=${HOME}/config/kadi.py' >> ~/.profile
  kadi $ echo 'test -z "${VIRTUAL_ENV}" && source ${HOME}/venv/bin/activate' >> ~/.profile

Shown below is an example of such a configuration file which can be used as a
starting point after changing some of the configuration values:

.. code-block:: python3

  import kadi.lib.constants as const
  AUTH_PROVIDERS = [{"type": "local"}]
  FOOTER_NAV_ITEMS = []
  MAIL_ERROR_LOGS = []
  MAIL_NO_REPLY = "no-reply@kadi4mat.example.edu"
  MAX_UPLOAD_SIZE = const.ONE_GB
  MAX_UPLOAD_USER_QUOTA = 10 * const.ONE_GB
  MISC_UPLOADS_PATH = "/opt/kadi/uploads"
  PLUGIN_CONFIG = {}
  PLUGINS = []
  SECRET_KEY = "<secret_key>"
  SENTRY_DSN = None
  SERVER_NAME = "kadi4mat.example.edu"
  SMTP_HOST = "localhost"
  SMTP_PASSWORD = ""
  SMTP_PORT = 25
  SMTP_USE_TLS = False
  SMTP_USERNAME = ""
  SQLALCHEMY_DATABASE_URI = "postgresql://kadi:<password>@localhost/kadi"
  STORAGE_PATH = "/opt/kadi/storage"

.. note::

  Please see the :ref:`Configuration <installation-configuration>` section for
  an explanation of all these configuration values. Make sure to adjust them
  before continuing, as the following steps require a correct configuration.

uWSGI
~~~~~

To generate a basic configuration for uWSGI, the Kadi CLI can be used:

.. code-block:: bash

  kadi $ kadi utils uwsgi --out kadi.ini

The generated configuration should be rechecked as further customization may be
necessary. Once the configuration is suitable, it can be enabled like this:

.. code-block:: bash

  sudo mv /opt/kadi/kadi.ini /etc/uwsgi/apps-available/
  sudo ln -s /etc/uwsgi/apps-available/kadi.ini /etc/uwsgi/apps-enabled/

Apache
~~~~~~

To generate a basic configuration for Apache, the Kadi CLI can be used:

.. code-block:: bash

  kadi $ kadi utils apache --out kadi.conf

The command will ask, among others, for a certificate and a key file, used to
encrypt the HTTP traffic using SSL/TLS (HTTPS). For testing or internal usage,
a self signed certificate may be used, which can be generated like this (note
that the ``<server_name>`` placeholder has to be substituted with the actual
name or IP of the host that was also configured via the Kadi4Mat configuration
file as ``SERVER_NAME``):

.. code-block:: bash

  sudo openssl req -x509 -nodes -days 365 -newkey rsa:4096 -keyout /etc/ssl/private/kadi.key -out /etc/ssl/certs/kadi.crt -subj "/CN=<server_name>" -addext "subjectAltName=DNS:<server_name>"

Alternatively, a real certificate may be obtained from `Let's Encrypt
<https://letsencrypt.org/>`__ for free. This is easiest done using their
`Certbot <https://certbot.eff.org/>`__ utility. For this to work however, the
server needs to be reachable externally on port 80.

The generated configuration should be rechecked as further customization may be
necessary. Once the configuration is suitable, it can be enabled like this:

.. code-block:: bash

  sudo mv /opt/kadi/kadi.conf /etc/apache2/sites-available/
  sudo a2dissite 000-default
  sudo a2ensite kadi

Finally, all additionally required Apache modules have to be activated as well:

.. code-block:: bash

  sudo a2enmod proxy_uwsgi ssl xsendfile headers

Celery
~~~~~~

To run Celery as a background service, it is recommended to set it up as a
systemd service. To generate the necessary unit file, the Kadi CLI can be used
again:

.. code-block:: bash

  kadi $ kadi utils celery --out kadi-celery.service

Celery beat, used to execute periodic tasks, needs its own unit file as well:

.. code-block:: bash

  kadi $ kadi utils celerybeat --out kadi-celerybeat.service

The generated configurations should be rechecked as further customization may
be necessary. Once both configurations are suitable, they can be enabled like
this:

.. code-block:: bash

  sudo mv /opt/kadi/kadi-celery.service /etc/systemd/system/
  sudo mv /opt/kadi/kadi-celerybeat.service /etc/systemd/system/

The services need access to the ``/var/log/celery`` directory to store logs,
which has to be created with the appropriate permissions:

.. code-block:: bash

  sudo mkdir /var/log/celery
  sudo chown -R kadi:www-data /var/log/celery

The user and group need to be set depending on what values were chosen in the
unit file.

To store temporary files, access to the ``/var/run/celery`` directory is needed
as well. Since all directories in ``/var/run/`` are ephemeral, they have to be
recreated on each reboot. To do so, an appropriate configuration has to be
created in ``/usr/lib/tmpfiles.d``:

.. code-block:: bash

  echo "d /run/celery 0755 kadi www-data" | sudo tee /usr/lib/tmpfiles.d/celery.conf > /dev/null
  sudo systemd-tmpfiles --create

Again, the user and group need to be set appropriately.

To let systemd know about the new services and also configure them to start
automatically when the system boots, the following commands can be used:

.. code-block:: bash

  sudo systemctl daemon-reload
  sudo systemctl enable kadi-celery kadi-celerybeat

Additionally, a logrotate config file may be created to make sure that any log
files created by Celery are rotated and compressed once a week:

.. code-block:: bash

  echo -e "/var/log/celery/*.log {\n  copytruncate\n  compress\n  delaycompress\n  missingok\n  notifempty\n  rotate 10\n  weekly\n}" | sudo tee /etc/logrotate.d/celery > /dev/null

Setting up the application
--------------------------

Before the application can be used, some initialization steps have to be done
using the Kadi CLI again:

.. code-block:: bash

  kadi $ kadi db init     # Initialize the database
  kadi $ kadi search init # Initialize the search indices

Finally, all new and modified services have to be restarted:

.. code-block:: bash

  sudo systemctl restart apache2 uwsgi kadi-celery kadi-celerybeat

Accessing Kadi4Mat
------------------

Kadi4Mat should now be reachable at ``https://<server_name>``. Again, the
``<server_name>`` placeholder has to be substituted with the actual name or IP
of the host that was also configured via the Kadi4Mat configuration file as
``SERVER_NAME``.

To be able to access Kadi4Mat from a different machine, make sure that any
firewall that may run on the server does not block access to ports 80 and 443.
