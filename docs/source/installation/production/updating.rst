Updating the application
========================

.. note::

  It is generally recommended to create an up-to-date backup of at least the
  database before updating the application.

When updating Kadi4Mat, it is recommended to first stop the Celery services and
afterwards the web and application server:

.. code-block:: bash

  sudo systemctl stop kadi-celery kadi-celerybeat
  sudo systemctl stop apache2 uwsgi

Afterwards, the application can be updated:

.. code-block:: bash

  sudo su - kadi      # Switch to the kadi user
  pip install kadi -U # Update the application code
  kadi db upgrade     # Upgrade the database schema

Finally, all services can be started again:

.. code-block:: bash

  sudo systemctl start apache2 uwsgi kadi-celery kadi-celerybeat
