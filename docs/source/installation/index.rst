Installation
============

This chapter contains instructions on how to install Kadi4Mat, either for
development or in a production environment, how to configure it afterwards as
well as how to install and configure plugins. The installation has currently
mainly been tested under Debian 10 (Buster), which the installation
instructions and scripts are also based on. However, other current Debian-based
distributions like Ubuntu (>= 18) should work the same way.

.. toctree::
   :maxdepth: 2

   Development <development/index>
   Production <production/index>
   Configuration <configuration>
   Plugins <plugins>
