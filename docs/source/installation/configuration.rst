.. _installation-configuration:

Configuration
=============

This section explains the currently most important configuration values which
need to be set in order to make Kadi4Mat work, including those where it may
make sense to change the default values.

Which of the configuration values need to be set depends on the method of
installation. It is therefore recommended to have a working Kadi4Mat
installation, either for development or in a production environment, before
reading through this section. Furthermore, some installation instructions may
also include example configuration files as a reference, e.g. when
:ref:`configuring Kadi4Mat in a production environment
<installation-production-manual-configuration-kadi4mat>`.

Note that the configuration file of Kadi4Mat is a normal Python file. This
means that it always has to be syntactically correct, but it also allows
imports, calculations and all other Python features. When changing the
configuration file while Kadi4Mat is running, it is recommended to restart
the application (similar to performing an update).

Some configuration aspects may also be done via the graphical sysadmin web
interface, which is only available to users marked as sysadmin. To initially
set a user as sysadmin, the Kadi CLI can be used:

.. code-block:: bash

  kadi users sysadmin <user_id>

Authentication
--------------

The configuration concerning authentication is explained separately, as it is a
bit more complex than other configuration values.

.. data:: AUTH_PROVIDERS

  This configuration value specifies the authentication providers to be used
  for logging in to Kadi4Mat. One or more providers can be specified as a list
  of dictionaries, where each dictionary needs to at least contain the type of
  the authentication provider. The following example enables all authentication
  providers that currently exist:

  .. code-block:: python3

    AUTH_PROVIDERS = [
      {"type": "local"},
      {"type": "ldap"},
      {"type": "shib"},
    ]

  Each authentication provider also contains various specific configuration
  options, which are left to their default values in the above example. The
  following sections describe each provider and its options in more detail.
  Note that the listed configuration values are the default options of each
  respective provider.

Local authentication
~~~~~~~~~~~~~~~~~~~~

Accounts based on local authentication are managed by Kadi4Mat itself.
Therefore, this option requires separate accounts to use Kadi4Mat, but is also
the easiest option to set up and use. Local users can also change their own
password and email address.

.. code-block:: python3

  {
      # The type of the authentication provider.
      "type": "local",
      # The title of the authentication provider that will be shown on the
      # login page.
      "title": "Login with credentials",
      # Whether to allow users that can access the website of Kadi4Mat to
      # register their own accounts by specifying their desired username,
      # display name, email address and password.
      "allow_registration": False,
      # Whether email confirmation through the website of Kadi4Mat is
      # required before any of its features can be used. In the future, email
      # confirmation may be used and configured in a more fine granular way.
      "email_confirmation_required": False,
  }

If not allowing users to register their own accounts, local accounts can be
created manually via the Kadi CLI:

.. code-block:: bash

  kadi users create

Sysadmins can also create users via the graphical sysadmin web interface.

LDAP authentication
~~~~~~~~~~~~~~~~~~~

Accounts based on LDAP authentication are managed by a directory service
implementing the LDAP protocol. Therefore, this option allows to use existing
user accounts. LDAP users cannot change their own email address, but may be
able to change their password through Kadi4Mat (in which case, the same
password checks as for local accounts are performed). Email addresses of LDAP
accounts are confirmed by default.

.. code-block:: python3

  {
      # The type of the authentication provider.
      "type": "ldap",
      # The title of the authentication provider that will be shown on the
      # login page.
      "title": "Login with LDAP",
      # The IP or hostname of the LDAP server.
      "host": "",
      # The port of the the LDAP server to connect with. Besides the default
      # port 389, port 636 is generally used together with SSL.
      "port": 389,
      # The encryption method to use. Can be set to 'ldaps' to use SSL or
      # 'starttls' to use STARTTLS.
      "encryption": None,
      # Whether to validate the server's SSL certificate if an encryption
      # method is set.
      "validate_cert": True,
      # The based DN where users are stored. Will be used to bind with a user
      # for authentication and also to search additional user attributes after
      # a successful bind.
      "users_dn": "",
      # The full DN of a user that should be used to perform any LDAP
      # operations after a successful bind. Per default, the bound (i.e.
      # authenticated) user will be used for these operations.
      "bind_user": None,
      # The password of the 'bind_user'.
      "bind_pw": None,
      # The LDAP attribute name for the username.
      "username_attr": "uid",
      # The LDAP attribute name for the email.
      "email_attr": "mail",
      # The LDAP attribute name for the display name. The username will be used
      # as fallback if not available.
      "displayname_attr": "displayName",
      # The LDAP attribute name for the first name. Only used if no display
      # name is available.
      "firstname_attr": "givenName",
      # The LDAP attribute name for the surname. Only used if no display name
      # is available.
      "lastname_attr": "sn",
      # Whether to allow LDAP users to change their password via Kadi4Mat. This
      # uses the LDAP Password Modify Extended Operation to perform the
      # password change, so any password hashing is done server-side.
      "allow_password_change": False,
      # Whether the standard Password Modify Extended Operation is supported by
      # the LDAP server. If not, an Active Directory is assumed.
      "supports_std_exop": True,
      # Whether to send the old password when changing it, which might be
      # required for some LDAP servers.
      "send_old_password": False,
  }

Shibboleth authentication
~~~~~~~~~~~~~~~~~~~~~~~~~

Accounts based on Shibboleth authentication are managed by one or more
(generally scientific) institutions. Therefore, this option allows to use
existing user accounts. Shibboleth users cannot change their own email address
or password. Email addresses of Shibboleth accounts are confirmed by default.

Configuring Shibboleth authentication is currently not described in this
documentation, as it requires additional dependencies, setup and configuration
that also depends on the specific environment and Identity Providers that are
to be used for authentication.

Other settings
--------------

.. data:: FOOTER_NAV_ITEMS

  This configuration value allows to easily specify additional navigation items
  to be listed on the left side of the navigation footer. Besides the URL for
  the navigation item, a title has to be provided in one or more languages. The
  default language (``"en"``) always has to be provided as it will also be used
  as fallback.

  For example:

  .. code-block:: python3

    FOOTER_NAV_ITEMS = [
        ("https://example.com/legals", {"en": "Legals", "de": "Impressum"}),
        ("https://example.com/privacy", {"en": "Privacy policy", "de": "Datenschutz"}),
    ]

.. data:: MAIL_ERROR_LOGS

  This configuration value allows to specify a list of email addresses which
  will receive logs of unexpected/uncaught errors and exceptions using Python's
  ``SMTPHandler``. As this will send an email for each individual error,
  setting up Sentry may be the preferred way for error monitoring (see
  ``SENTRY_DSN``).

.. data:: MAIL_NO_REPLY

  The email address that will be used to send no-reply emails from, e.g. for
  confirming email addresses or password reset requests for local accounts.

  Defaults to ``"no-reply@<fqdn>"``, where ``<fqdn>`` is the fully qualified
  domain name of the server.

.. data:: MAX_UPLOAD_SIZE

  The maximum size of a single locally stored file upload in bytes. Setting
  this configuration value to ``None`` will remove the size limit altogether.

  Defaults to ``1000000000`` (1GB).

.. data:: MAX_UPLOAD_USER_QUOTA

  The maximum total size of locally stored files a single user can upload.
  Setting this configuration value to ``None`` will remove the size limit
  altogether. Currently, this cannot be set on a per-user basis.

  Defaults to ``10000000000`` (10GB).

.. data:: MISC_UPLOADS_PATH

  Specifies the path that all miscellaneous uploads are stored in (e.g. profile
  or group pictures). Permissions for this directory need to be set
  accordingly, e.g.:

  .. code-block:: bash

    sudo chown kadi:www-data /opt/kadi/uploads
    sudo chmod 750 /opt/kadi/uploads

.. data:: PLUGIN_CONFIG; PLUGINS

  These configuration values are used to enable and configure plugins. See the
  :ref:`Plugins <installation-plugins>` section for more information.

.. data:: SECRET_KEY

  The secret key is used for most things that require encryption in Kadi4Mat,
  so it should be an appropriately secure value. The Kadi CLI offers a command
  that can be used to generate a suitable value:

  .. code-block:: bash

    kadi utils secret-key

.. data:: SENTRY_DSN

  A Sentry DSN (Data Source Name) which can be used to integrate the `Sentry
  <https://sentry.io/>`__ error monitoring tool with Kadi4Mat.

.. data:: SERVER_NAME

  The name or IP of the host that Kadi4Mat runs on.

.. data:: SMTP_HOST; SMTP_PASSWORD; SMTP_PORT; SMTP_USE_TLS; SMTP_USERNAME

  These values specify the SMTP connection needed to send emails, most
  importantly the SMTP host (``SMTP_HOST``) and port (``SMTP_PORT``). If the
  SMTP server requires authentication, the username and password can be
  specified using ``SMTP_USERNAME`` and ``SMTP_PASSWORD``. If ``SMTP_USE_TLS``
  is enabled, STARTTLS will be used for an encrypted SMTP connection.

.. data:: SQLALCHEMY_DATABASE_URI

  This configuration value specifies the database connection to use, in the
  form of ``"postgresql://<user>:<password>@<host>/<database>"``.

.. data:: STORAGE_PATH

  Specifies the path that all local files and uploads of records will be stored
  in. Permissions for this directory need to be set accordingly, e.g.:

  .. code-block:: bash

    sudo chown kadi:www-data /opt/kadi/storage
    sudo chmod 750 /opt/kadi/storage
