Users
=====

GET
---

.. autoflask:: kadi.wsgi:app
   :packages: kadi.modules.accounts.api
   :methods: get
   :autoquickref:
