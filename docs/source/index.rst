.. image:: /images/kadi.png
   :width: 175 px
   :align: right

Welcome to Kadi4Mat's documentation!
====================================

**Kadi4Mat** is the **Karlsruhe Data Infrastructure for Materials Science**, an
open source software for managing research data. The goal of this project is to
combine the ability to manage and exchange data, the *repository*, with the
possibility to analyze, visualize and transform said data, the *electronic lab
notebook (ELN)*. More information can be found `here
<https://kadi.iam-cms.kit.edu/>`__.

This documentation contains instructions on how to install Kadi4Mat, either for
development or in a production environment, some general topics and useful
tools about developing or contributing to Kadi4Mat, the usage and endpoints of
the HTTP API Kadi4Mat offers as well as an API reference and overview of the
project and source code itself.

.. note::

  This documentation is still a work in progress and does not cover all aspects
  and details of Kadi4Mat yet.

Table of contents
=================

.. toctree::
   :maxdepth: 2

   Installation <installation/index>
   Development <development/index>
   HTTP API <httpapi/index>
   API reference <apiref/index>
